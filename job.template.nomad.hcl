job "trace-management" {
  name        = "Trace Management (Tempo)"
  type        = "service"
  region      = "se"
  datacenters = ["soc"]
  namespace   = "c3-monitoring"

  group "tempo" {
    count = 1

    consul {}

    volume "tempo-data" {
      type            = "csi"
      source          = "tempo-data"
      attachment_mode = "file-system"
      access_mode     = "single-node-writer"
      per_alloc       = true
    }

    network {
      mode = "bridge"
    }

    service {
      provider = "consul"
      name     = "tempo"
      port     = "3200"
      task     = "tempo"

      connect {
        sidecar_service {}
      }

      check {
        expose   = true
        name     = "Application Ready Status"
        type     = "http"
        path     = "/ready"
        interval = "10s"
        timeout  = "3s"
      }
    }

    // service {
    //   provider = "consul"
    //   name     = "tempo-zipkin"
    //   port     = "9411"
    //   task     = "tempo"

    //   connect {
    //     sidecar_service {}
    //   }

    //   // check {
    //   //   name     = "alive"
    //   //   type     = "tcp"
    //   //   port     = "http"
    //   //   interval = "10s"
    //   //   timeout  = "2s"
    //   // }
    // }

    task "tempo" {
      driver = "docker"
      user   = "[[ .defaultUserId ]]"

      config {
        image = "[[ .tempoImageName ]]"

        args = ["-config.file=/etc/tempo.yaml"]

        volumes = [
          "local/tempo/tempo.yaml:/etc/tempo.yaml"
        ]
      }

      volume_mount {
        volume      = "tempo-data"
        destination = "/tmp/tempo-data"
        read_only   = false
      }

      template {
        data = <<EOH
[[ fileContents "./config/tempo.template.yaml" ]]
        EOH

        destination = "local/tempo/tempo.yaml"
      }
    }
  }

  reschedule {
    delay          = "10s"
    delay_function = "exponential"
    max_delay      = "10m"
    unlimited      = true
  }

  update {
    health_check      = "checks"
    min_healthy_time  = "10s"
    healthy_deadline  = "10m"
    progress_deadline = "15m"
    auto_revert       = true
  }

  meta {
    gitSha      = "[[ .gitSha ]]"
    gitBranch   = "[[ .gitBranch ]]"
    pipelineId  = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId   = "[[ .projectId ]]"
    projectUrl  = "[[ .projectUrl ]]"
    statefull   = "true"
  }
}
